package ru.trifonov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
//@XmlRootElement(name = "project")
public final class ProjectDTO extends AbstractDTO {
    public ProjectDTO(
            @NotNull String name, @NotNull String description,
            @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.name = name;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public ProjectDTO(
            @NotNull String id, @NotNull String name,
            @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  STATUS " + status +
                "  PROJECT CREATE DATE " + createDate +
                "  PROJECT BEGIN DATE " + beginDate +
                "  PROJECT END DATE " + endDate;
    }
}