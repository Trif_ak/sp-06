package ru.trifonov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
//@XmlRootElement(name = "task")
public final class TaskDTO extends AbstractDTO {
    public TaskDTO(
            @NotNull String projectId, @NotNull String name,
            @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.projectId = projectId;
        this.name = name;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public TaskDTO(
            @NotNull String id, @NotNull String projectId, @NotNull String name,
            @NotNull String description,
            @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.id = id;
        this.projectId = projectId;
        this.name = name;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @NotNull
    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  STATUS " + status +
                "  TASK CREATE DATE " + createDate +
                "  TASK BEGIN DATE " + beginDate +
                "  TASK END DATE " + endDate;
    }
}
