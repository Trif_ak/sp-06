package ru.trifonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.dto.TaskDTO;

import java.util.List;

@Service
public final class TaskService extends AbstractService implements ITaskService {
    @NotNull
    private final RestTemplate restTemplate;
    @Autowired
    public TaskService(@NotNull final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    @SneakyThrows
    public void insert(
            @Nullable final String projectId, @Nullable final String name,
            @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct projectid.");
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @NotNull final TaskDTO taskDTO = new TaskDTO(
                projectId,
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        restTemplate.postForEntity(
                "http://localhost:8080/rest-server/task",
                taskDTO,
                TaskDTO.class
        );
    }

    @Override
    @SneakyThrows
    public void update(
            @Nullable final String id, @Nullable final String projectId, @Nullable final String name,
            @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct projectid.");
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @NotNull final TaskDTO taskDTO = new TaskDTO(
                id,
                projectId,
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        restTemplate.put("http://localhost:8080/rest-server/task", taskDTO);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final ResponseEntity<List<TaskDTO>> responseEntity = restTemplate.exchange (
                "http://localhost:8080/rest-server/tasks",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<TaskDTO>>() {}
        );
        @Nullable List<TaskDTO> tasksDTO = responseEntity.getBody();
        if (tasksDTO == null || tasksDTO.isEmpty()) throw new NullPointerException("Tasks not found.");
        return tasksDTO;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct project id.");
        @NotNull final ResponseEntity<List<TaskDTO>> responseEntity = restTemplate.exchange (
                "http://localhost:8080/rest-server/tasks/{id}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<TaskDTO>>() {},
                projectId
        );
        @Nullable List<TaskDTO> tasksDTO = responseEntity.getBody();
        if (tasksDTO == null || tasksDTO.isEmpty()) throw new NullPointerException("Tasks not found.");
        return tasksDTO;
    }

    @NotNull
    @Override
    public TaskDTO find(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        @Nullable TaskDTO taskDTO = restTemplate.getForObject(
                "http://localhost:8080/rest-server/task/{id}",
                TaskDTO.class,
                id
        );
        if (taskDTO == null) throw new NullPointerException("Task not found.");
        return taskDTO;
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        restTemplate.delete("http://localhost:8080/rest-server/task/{id}", id);
    }
}
