package ru.trifonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.dto.ProjectDTO;

import java.util.List;

@Service
public final class ProjectService extends AbstractService implements IProjectService {
    @NotNull
    private final RestTemplate restTemplate;
    @Autowired
    public ProjectService(@NotNull final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    @SneakyThrows
    public void insert(
            @Nullable final String name, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        restTemplate.postForEntity(
                "http://localhost:8080/rest-server/project",
                projectDTO,
                ProjectDTO.class
        );
    }

    @SneakyThrows
    @Override
    public void update(
            @Nullable final String id, @Nullable final String name,
            @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(
                id,
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        restTemplate.put("http://localhost:8080/rest-server/project", projectDTO);
    }

    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final ResponseEntity<List<ProjectDTO>> responseEntity = restTemplate.exchange (
                "http://localhost:8080/rest-server/projects",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ProjectDTO>>() {}
        );
        @Nullable List<ProjectDTO> projectsDTO = responseEntity.getBody();
        if (projectsDTO == null  || projectsDTO.isEmpty()) throw new NullPointerException("Projects not found.");
        return projectsDTO;
    }

    @Override
    public ProjectDTO find(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        @Nullable ProjectDTO projectDTO = restTemplate.getForObject(
                "http://localhost:8080/rest-server/project/{id}",
                ProjectDTO.class,
                id
        );
        if (projectDTO == null) throw new NullPointerException("Projects not found.");
        return projectDTO;
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        restTemplate.delete("http://localhost:8080/rest-server/project/{id}", id);
    }
}