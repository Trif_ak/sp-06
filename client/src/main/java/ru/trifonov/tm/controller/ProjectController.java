package ru.trifonov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.dto.ProjectDTO;

import java.util.List;

@Controller
public final class ProjectController {
    @NotNull
    IProjectService projectService;

    @Autowired
    public ProjectController(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @PostMapping("/project-create")
    public String createProjectPost(
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("beginDate") @Nullable final String beginDate,
            @ModelAttribute("endDate") @Nullable final String endDate
    ) {
        projectService.insert(name, description, beginDate, endDate);
        return "redirect:/projects";
    }

    @GetMapping("/project-create")
    public String createProjectGet() {
        return "projectCreate";
    }

    @PostMapping("/project-edit")
    public String projectEditPost(
            @ModelAttribute("id") @Nullable final String id,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("beginDate") @Nullable final String dateOfBegin,
            @ModelAttribute("endDate") @Nullable final String dateOfEnd
    ){
        projectService.update(id, name, description, dateOfBegin, dateOfEnd);
        return "redirect:/projects";
    }

    @GetMapping("/project-edit")
    public String projectEdit(
            @NotNull final Model model,
            @RequestParam(name = "id") final String id
    ) {
        model.addAttribute("id", id);
        return "projectEdit";
    }

    @GetMapping("/projects")
    public String findAll(@NotNull final Model model) {
        @NotNull final List<ProjectDTO> projects = projectService.findAll();
        model.addAttribute("projectList", projects);
        return "projects";
    }

    @GetMapping("/project-view")
    public String findById(@NotNull final Model model, @RequestParam(name = "id") @Nullable final String id) {
        @NotNull final ProjectDTO projectDTO = projectService.find(id);
        model.addAttribute("project", projectDTO);
        return "projectView";
    }

    @GetMapping("/project-delete")
    public String delete(@RequestParam(name = "id") @Nullable final String id) {
        projectService.delete(id);
        return "redirect:/projects";
    }
}
