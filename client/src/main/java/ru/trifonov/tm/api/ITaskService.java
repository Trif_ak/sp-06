package ru.trifonov.tm.api;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskService {
    @SneakyThrows
    void insert(
            @Nullable String projectId, @Nullable String name,
            @Nullable String description, @Nullable String beginDate, @Nullable String endDate
    );

    @SneakyThrows
    void update(
            @Nullable String id, @Nullable String projectId, @Nullable String name,
            @Nullable String description, @Nullable String beginDate, @Nullable String endDate
    );

    List<TaskDTO> findAll();

    @NotNull List<TaskDTO> findAllByProjectId(@Nullable String projectId);

    TaskDTO find(@Nullable String id);

    void delete(@Nullable String id);
}
