package ru.trifonov.tm.api;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectService {
    @SneakyThrows
    void insert(
            @Nullable String name, @Nullable String description,
            @Nullable String beginDate, @Nullable String endDate
    );

    void update(
            @Nullable String id, @Nullable String name, @Nullable String description,
            @Nullable String beginDate, @Nullable String endDate
    );
    List<ProjectDTO> findAll();

    ProjectDTO find(@Nullable String id);

    void delete(@Nullable String id);
}
