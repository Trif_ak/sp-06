package ru.trifonov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.dto.ProjectDTO;

import java.util.List;

@RestController
public final class ProjectController {
    @NotNull
    IProjectService projectService;

    @Autowired
    public ProjectController(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @NotNull
//    @ResponseBody
    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_XML_VALUE)
    public List<ProjectDTO> findAll() {
        return projectService.findAll();
    }

    @NotNull
//    @ResponseBody
    @GetMapping(value = "/project/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    public ProjectDTO findById(@PathVariable(name = "id") @Nullable final String id) {
        return projectService.find(id);
    }

//    @ResponseBody
    @PostMapping(value = "/project")
    public void createProjectPost(@RequestBody @Nullable final ProjectDTO projectDTO) {
        projectService.insert(projectDTO);
    }

//    @ResponseBody
//    @PutMapping(value = "/project", produces = MediaType.APPLICATION_XML_VALUE)
    @PutMapping(value = "/project")
    public void projectEditPost(@RequestBody @Nullable final ProjectDTO projectDTO){
        projectService.update(projectDTO);
    }

//    @ResponseBody
    @DeleteMapping(value = "/project/{id}")
    public void delete(@PathVariable(name = "id") @Nullable final String id) {
        projectService.delete(id);
    }
}
