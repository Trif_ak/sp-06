package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.dto.ProjectDTO;
import ru.trifonov.tm.dto.TaskDTO;
import ru.trifonov.tm.model.Task;
import ru.trifonov.tm.repository.ITaskRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public final class TaskService extends AbstractService implements ITaskService {
    @NotNull
    ITaskRepository taskRepository;
    @NotNull
    IProjectService projectService;

    @Autowired
    public TaskService(@NotNull final ITaskRepository taskRepository, @NotNull final IProjectService projectService) {
        this.taskRepository = taskRepository;
        this.projectService = projectService;
    }

    @Override
    public void insert(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) throw new NullPointerException("Something wrong with TaskDTO.");
        @NotNull final Task task = dtoToEntity(taskDTO);
        taskRepository.save(task);
    }

    @Override
    public void update(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) throw new NullPointerException("Something wrong with TaskDTO.");
        @NotNull final Task correctTask = dtoToEntity(taskDTO);
        @NotNull final Task currentTask = dtoToEntity(find(taskDTO.getId()));
        currentTask.setName(correctTask.getName());
        currentTask.setDescription(correctTask.getDescription());
        currentTask.setBeginDate(correctTask.getBeginDate());
        currentTask.setEndDate(correctTask.getEndDate());
        taskRepository.save(currentTask);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @Nullable List<Task> tasks = taskRepository.findAll();
        if (tasks == null || tasks.isEmpty()) throw new NullPointerException("Tasks not found.");
        @NotNull List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(entityToDTO(task));
        }
        return tasksDTO;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct project id.");
        @Nullable List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        if (tasks == null || tasks.isEmpty()) throw new NullPointerException("Tasks not found.");
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(entityToDTO(task));
        }
        return tasksDTO;
    }

    @NotNull
    @Override
    public TaskDTO find(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        System.out.println(id);
        if (!taskRepository.findById(id).isPresent()) throw new NullPointerException("Task not found.");
        @NotNull Task task = taskRepository.findById(id).get();
        @NotNull TaskDTO taskDTO = entityToDTO(task);
        return taskDTO;
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        taskRepository.deleteById(id);
    }

    @NotNull
    @Override
    public TaskDTO entityToDTO(@NotNull final Task task) {
        @NotNull final TaskDTO taskDTO = new TaskDTO(
                task.getId(),
                task.getProject().getId(),
                task.getName(),
                task.getDescription(),
                task.getStatus(),
                task.getBeginDate(),
                task.getEndDate(),
                task.getCreateDate()
        );
        return taskDTO;
    }

    @NotNull
    @Override
    public Task dtoToEntity(@NotNull final TaskDTO taskDTO) {
        @NotNull final ProjectDTO projectDTO = projectService.find(taskDTO.getProjectId());
        @NotNull final Task task = new Task(
                taskDTO.getId(),
                projectService.dtoToEntity(projectDTO),
                taskDTO.getName(),
                taskDTO.getDescription(),
                taskDTO.getStatus(),
                taskDTO.getBeginDate(),
                taskDTO.getEndDate(),
                taskDTO.getCreateDate()
        );
        return task;
    }
}
