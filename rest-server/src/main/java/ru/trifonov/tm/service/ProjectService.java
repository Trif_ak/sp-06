package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.dto.ProjectDTO;
import ru.trifonov.tm.model.Project;
import ru.trifonov.tm.repository.IProjectRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public final class ProjectService extends AbstractService implements IProjectService {
    @NotNull
    IProjectRepository projectRepository;

    @Autowired
    public ProjectService(@NotNull IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void insert(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) throw new NullPointerException("Something wrong with ProjectDTO.");
        @NotNull final Project project = dtoToEntity(projectDTO);
        projectRepository.save(project);
    }

    @Override
    public void update(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) throw new NullPointerException("Something wrong with ProjectDTO.");
        @NotNull final Project correctProject = dtoToEntity(projectDTO);
        @NotNull final Project currentProject = dtoToEntity(find(projectDTO.getId()));
        currentProject.setName(correctProject.getName());
        currentProject.setDescription(correctProject.getDescription());
        currentProject.setBeginDate(correctProject.getBeginDate());
        currentProject.setEndDate(correctProject.getEndDate());
        projectRepository.save(currentProject);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        @Nullable List<Project> projects = projectRepository.findAll();
        if (projects.isEmpty()) throw new NullPointerException("Projects not found.");
        @NotNull List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDTO.add(entityToDTO(project));
        }
        return projectsDTO;
    }

    @NotNull
    @Override
    public ProjectDTO find(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        @NotNull Project project = projectRepository.findById(id).get();
        return entityToDTO(project);
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        projectRepository.deleteById(id);
    }

    @NotNull
    @Override
    public ProjectDTO entityToDTO(@NotNull final Project project) {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(
                project.getId(),
                project.getName(),
                project.getDescription(),
                project.getStatus(),
                project.getBeginDate(),
                project.getEndDate(),
                project.getCreateDate()
        );
        return projectDTO;
    }

    @NotNull
    @Override
    public Project dtoToEntity(@NotNull final ProjectDTO projectDTO) {
        @NotNull final Project project = new Project(
                projectDTO.getId(),
                projectDTO.getName(),
                projectDTO.getDescription(),
                projectDTO.getStatus(),
                projectDTO.getBeginDate(),
                projectDTO.getEndDate(),
                projectDTO.getCreateDate()
        );
        return project;
    }
}