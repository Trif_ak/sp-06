package ru.trifonov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.util.IdUtil;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractDTO implements Serializable {
    @NotNull
    protected String id = IdUtil.getUUID();
}
